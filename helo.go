package main

import (
	// "errors"
	// "fmt"
	"html/template"
	"net/http"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
	// "math"
	// "time"
	// "math/rand"
)

//---------------------------------------------
// func sum(x int, y int) int {
// 	return x + y
// }
//------------------------------
// func sqrt(x float64) (float64, error) {
// 	if x < 0 {
// 		return 0, errors.New("this is number is less than zero")
// 	}
// 	return math.Sqrt(x), nil
// }
//---------------------------------------
// type person struct {
// 	name string
// 	age  int
// }
//----------------------------------------
// func inc(x *int) {
// 	*x++
// }
//-----------------------------------
var client *redis.Client
var store = sessions.NewCookieStore([]byte("t0p-s3cr3t"))
var templates *template.Template

func main() {
	client = redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	})
	templates = template.Must(template.ParseGlob("templates/*.html"))
	r := mux.NewRouter()
	//http not nedd when use mux
	// http.HandleFunc("/home", homeHandler)
	r.HandleFunc("/", indexgetHandler).Methods("GET")
	r.HandleFunc("/", indexpostHandler).Methods("POST")
	r.HandleFunc("/login", logingetHandler).Methods("GET")
	r.HandleFunc("/login", loginpostHandler).Methods("POST")
	// r.HandleFunc("/tests", testsgetHandler).Methods("GET")
	r.HandleFunc("/regester", regstergetHandler).Methods("GET")
	r.HandleFunc("/regester", regsterpostHandler).Methods("POST")

	r.HandleFunc("/home", index2getHandler).Methods("GET")
	// r.HandleFunc("/about", aboutHandler).Methods("GET")
	fs := http.FileServer(http.Dir("./test/"))
	r.PathPrefix("/test/").Handler(http.StripPrefix("/test/", fs))
	http.Handle("/", r) // if not use progam not run

	http.ListenAndServe(":2020", nil)
}
func indexgetHandler(w http.ResponseWriter, r *http.Request) {
	sessions, _ := store.Get(r, "session")
	_, ok := sessions.Values["username"]
	if !ok {
		http.Redirect(w, r, "/login", 302)
		return
	}
	comments, err := client.LRange("comments", 0, 10).Result()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server eroor"))
		return
	}
	templates.ExecuteTemplate(w, "index.html", comments)
}
func indexpostHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	comment := r.PostForm.Get("comment")
	err := client.LPush("comments", comment).Err()
	//  client.LPush("comments", comment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server eroor"))
		return
	}
	http.Redirect(w, r, "/", 302)

}

func logingetHandler(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "login.html", nil)

}
func index2getHandler(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "index22.html", nil)

}

func loginpostHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := r.PostForm.Get("username")
	passsword := r.PostForm.Get("password")
	hash, err := client.Get("user:" + username).Bytes()
	if err == redis.Nil {
		templates.ExecuteTemplate(w, "login.html", "unknown user")
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server eroor"))
		return
	}
	err = bcrypt.CompareHashAndPassword(hash, []byte(passsword))
	if err != nil {
		templates.ExecuteTemplate(w, "login.html", "invalid login")
		return
	}
	session, _ := store.Get(r, "session")
	session.Values["username"] = username
	session.Save(r, w)
}

// func testsgetHandler(w http.ResponseWriter, r *http.Request) {
// 	session, _ := store.Get(r, "session")
// 	untyped, ok := session.Values["username"]
// 	if !ok {
// 		return
// 	}
// 	username, ok := untyped.(string)
// 	if !ok {
// 		return
// 	}
// 	w.Write([]byte(username))

// }
func regstergetHandler(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "regster.html", nil)

}
func regsterpostHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := r.PostForm.Get("username")
	password := r.PostForm.Get("passsword")
	cost := bcrypt.DefaultCost
	hash, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server eroor"))
		return
	}
	client.Set("user:"+username, hash, 0)
	http.Redirect(w, r, "/login", 302)
}

// func aboutHandler(w http.ResponseWriter, r *http.Request) {
// 	fmt.Fprintf(w, "heloo to this page is about")
// }

//---------------------------------
// i := 7
// inc(&i)
// fmt.Println(i)

//------------------------------------
// p := person{name: "poriya", age: 13}
// t := person{name: "taha", age: 133}
// a := person{name: "ali", age: 163}
// fmt.Println(p,t,a)
//-------------------------------------
// result, err := sqrt(-7)
// if err != nil {
// 	fmt.Println(err)
// }
// // else{
// // 	fmt.Println(err)
// // }
// fmt.Println(result)
//---------------------------------------------------
// arr := make(map[string]string)
// arr["poriya"] = "daliry"
// arr["ali"] = "mohamdi"
// for key, value := range arr {
// 	fmt.Println("key is", key, "value", value)
// }
//---------------------------------------------------------
// arr := []string{"poriya", "ali", "taha", "mohamad"}
// for index, value := range arr {
// 	fmt.Println("index is", index, "value", value)
// }
//---------------------------------------------------------
// for i := 1; i < 10; i++ {
// 	fmt.Println("number is ", i, "is ")
// }
//-------------------------------------------------------
// people := make(map[string]int)
// people["poriya"] = 22
// people["taha"] = 14
// people["ali"] = 33
// delete(people, "ali")
// fmt.Println(people)
// fmt.Println(people["poriya"])
//-----------------------------------------------------------
// a := [5]int{3, 5, 67, 76, 43}
// // a = append(a, 12)
// fmt.Println(a)
// var a [5]int
// a[2] = 7
// fmt.Println(a)
// fmt.Println("Welcome to the playground!")
//-------------------------------------------------------------
// fmt.Println("The time is", time.Now())
//---------------------------------------------------
// // fmt.Println("My favorite number is", rand.Intn(20))
//--------------------------------------------------------
// fmt.Println("number is %g peombelm\n",math.Sqrt(5))

// func main() {
// 	x := 7
// 	// var y int =3
// 	// var z int =x+y
// 	// fmt.Println(z)
// 	if x > 6 {
// 		fmt.Println("more than 6")
// 	}

// }
